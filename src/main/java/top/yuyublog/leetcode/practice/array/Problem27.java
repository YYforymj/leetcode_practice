package top.yuyublog.leetcode.practice.array;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;

/**
 * @author yumj
 * @date 2021/12/1
 */
public class Problem27 {

    private static final int[] testCase = {0, 3, 2, 2, 3, 0, 4, 3};

    public static void main(String[] args) {
        Problem27.Solution solution = new Problem27.Solution();
        int length = solution.removeElement(testCase, 3);
        ArrayList<Integer> arrayList = new ArrayList<Integer>(testCase.length);
        for (int i = 0; i < length; i++) {
            arrayList.add(i, testCase[i]);
        }
        System.out.println(JSONObject.toJSONString(arrayList.subList(0, length)));
    }

    static class Solution {
        public int removeElement(int[] nums, int val) {
            int j = 0;
            for (int i = 0; i < nums.length; i++) {
                while (i < nums.length && nums[i] == val) {
                    i++;
                }
                if (i >= nums.length) {
                    break;
                }
                nums[j] = nums[i];
                j++;
            }
            return j;
        }
    }
}
