package top.yuyublog.leetcode.practice.array;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;

/**
 * 两数之和
 *
 * @author yumj
 * @date 2021/12/1
 */
public class Problem1 {
    private static final int[] testCase = {2, 7, 11, 15};

    static class Solution {
        public int[] twoSum(int[] nums, int target) {
            if (nums.length == 0) {
                return new int[]{};
            }
            HashMap<Integer, Integer> table = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                Integer pair = target - nums[i];
                if (table.containsKey(pair)) {
                    return new int[]{table.get(pair), i};
                }
                table.put(nums[i], i);
            }
            return null;
        }
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(JSONObject.toJSONString(solution.twoSum(testCase, 9)));
    }

}
