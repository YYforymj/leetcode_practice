package top.yuyublog.leetcode.practice.array;

import com.alibaba.fastjson.JSONObject;

/**
 * @author yumj
 * @date 2021/12/1
 */
public class Problem26 {
    private static final int[] testCase = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};

    static class Solution {
        public int removeDuplicates(int[] nums) {
            int j = 0;
            for(int i =0 ; i < nums.length;i++){
                if(nums[i] == nums[j]){
                    continue;
                }
                j++;
                nums[j] = nums[i];
            }
            return j + 1;
        }
    }
    public static void main(String[] args) {
        Problem26.Solution solution = new Problem26.Solution();
        System.out.println(JSONObject.toJSONString(solution.removeDuplicates(testCase)));
    }
}
